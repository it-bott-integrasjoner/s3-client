import pytest
from botocore.exceptions import ClientError

from s3_client import get_client, models


## Before run:
## Assumes that folders and files are created and uploaded to aws
## in the bucket(manually), aws should have same files as fixtures/downloads
## use already created bucket: kursinfo-itest


from s3_client.models import MoveFileReq


@pytest.mark.integration
def test_create_bucket(service_config):
    print("create bucket")
    client = get_client(service_config)
    created = client.create_bucket("kursinfo")
    assert created is not None


@pytest.mark.integration
def test_download_non_existing_file(service_config):
    client = get_client(service_config)

    with pytest.raises(ClientError):
        client.download(file_name="x")

@pytest.mark.integration
def test_download_existing_file(service_config):
    client = get_client(service_config)
    file = client.download(file_name="csv-archive/1/1.1/kurs-fullføringer3.csv")
    assert len(file) == 1


@pytest.mark.integration
def test_download_given_file_in_sub_folder(service_config):
    client = get_client(service_config)
    file_name = "kurs-fullføringer.csv"

    files = client.download(file_name=file_name, sub_folder="csv-import")
    with open(files[0], "rb") as file:
        lines = file.readlines()
    assert len(files) == 1
    assert service_config.s3.download_path + file_name in files[0]
    assert lines is not None


@pytest.mark.integration
def test_download_all_file_in_sub_folder(service_config):
    client = get_client(service_config)

    files = client.download(file_name=None, sub_folder="csv-import")
    assert len(files) == 2
    with open(files[0], "rb") as file:
        lines = file.readlines()
    file_name = "kurs-fullføringer.csv"
    assert service_config.s3.download_path + "csv-import/" + file_name in \
           files[0]
    assert lines is not None


@pytest.mark.integration
def test_download_non_existing_folder(service_config):
    client = get_client(service_config)

    with pytest.raises(ValueError):
        client.download(file_name=None, sub_folder="x")


@pytest.mark.integration
def test_download_all_nested_sub_folder(service_config):
    client = get_client(service_config)

    files = client.download(file_name=None, sub_folder="csv-archive/1/1.1")
    assert len(files) == 1
    for file in files:
        with open(file, "rb") as f:
            lines = f.readlines()
            assert lines is not None


@pytest.mark.integration
def test_download_all_nested_sub_folder(service_config):
    client = get_client(service_config)

    files = client.download(file_name=None, sub_folder="csv-archive")
    assert len(files) >= 3
    for file in files:
        with open(file, "rb") as f:
            lines = f.readlines()
            assert lines is not None


@pytest.mark.integration
def test_move(service_config):
    client = get_client(service_config)

    req_copy = MoveFileReq(file_src="csv-import/kurs-fullføringer.csv",
        file="csv-import/kurs-fullføringer.csv.org.csv")
    copy_backup = client.move(req_copy, del_src=False)

    move_to_archive = MoveFileReq(file_src="csv-import/kurs-fullføringer.csv",
                      file="csv-archive/kurs-fullføringer-2020-11-22.csv")
    moved = client.move(move_to_archive)

    assert moved == True

    req_copy = MoveFileReq(file_src="csv-import/kurs-fullføringer.csv.org.csv",
        file="csv-import/kurs-fullføringer.csv")

    copy_origin = client.move(req_copy)


    assert moved == True
    assert copy_backup == True
    assert copy_origin == True
