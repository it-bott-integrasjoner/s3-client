from unittest.mock import patch

import yaml
from s3_client import models, get_client, S3Client

from tests.conftest import load_file


def get_config():
    file = load_file("../config.example.yaml")
    configs = yaml.load(file, Loader=yaml.FullLoader)
    return models.S3ClientConfig(**configs)


def test_create_config_from_example():
    config = get_config()
    assert config is not None


@patch.object(S3Client, 'bo_res')
def test_create_client_raises_missing_required_folder(res_mock):
    config = get_config()
    client = get_client(config)
    assert client is not None
