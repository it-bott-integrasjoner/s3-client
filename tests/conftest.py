import os

import pytest
from pathlib import Path
import yaml

from s3_client import get_client, models
def load_file(name, rb="r"):
    file = get_file(name)
    with open(file, rb) as fi:
        return fi.read()


def get_file(name):
    print("loading file: ", name)
    file = os.path.join(os.path.dirname(__file__), name)
    file = Path(file)
    return file

@pytest.fixture
def service_config():
    file = load_file("../config.yaml")
    configs = yaml.load(file, Loader=yaml.FullLoader)
    return models.S3ClientConfig(**configs)

@pytest.fixture
def client(config: models.S3ClientConfig):
    return get_client(config)

@pytest.fixture
def client_mock(ex_config):
    class S3Client:
        pass

def pytest_collection_modifyitems(config, items):
    if config.option.keyword or config.option.markexpr:
        return
    skip_integration = pytest.mark.skip(
        reason='Not running with pytest -m "integration"'
    )
    for item in items:
        if 'integration' in item.keywords:
            item.add_marker(skip_integration)
