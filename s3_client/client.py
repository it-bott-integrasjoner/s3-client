import logging
import os
from typing import Optional

import boto3
from botocore.exceptions import ClientError

from s3_client import models
from s3_client.models import S3ClientConfig, MoveFileReq

logger = logging.getLogger(__name__)


class S3Client:
    bo_res = None

    def __init__(self, config: S3ClientConfig):
        self.config = config

        if self.bo_res is None:

            self.bo_res = boto3.resource("s3",
                endpoint_url=config.s3.aws_endpoint_url,
                region_name=config.s3.region_name,
                aws_access_key_id=config.s3.aws_access_key_id,
                aws_secret_access_key=config.s3.aws_secret_access_key)

        if not config.s3.download_path.startswith("/"):
            self.abs_download_folder = os.path.dirname(
                __file__) + "/../" + config.s3.download_path
        else:
            self.abs_download_folder = config.s3.download_path

        try:
            os.makedirs(self.abs_download_folder, exist_ok=True)
        except IOError as error:
            logger.error(f"Cant create folder: {config.s3.download_path} ",
                exc_info=error)
            raise

        if not os.access(self.abs_download_folder, os.W_OK):
            raise ValueError(
                f"Download folder: {config.s3.download_path}" +
                "is not writable")

        logger.info("Using download folder: %s", self.abs_download_folder)
        for folder in config.s3.required_folders:
            folders = list(self.bo_res.Bucket(config.s3.bucket).objects.filter(
                Prefix=folder))
            if len(folders) == 0:
                raise ValueError(f"Could not find required folder: {folder}")

        logger.info("s3 initialized to: %s", config.s3.aws_endpoint_url)

    def create_bucket(self, name: str):
        """Create a bucket"""
        created = self.bo_res.meta.client.create_bucket(Bucket=name)
        return created

    def download(self, bucket_name: Optional[str] = None,
            sub_folder: Optional[str] = None,
            file_name: Optional[str] = None):
        """Download given files in specified folder, in bucket.
        When sub_folder is None, all the files are downloaded
        When given file_name that specific file is download"""

        bucket_name = bucket_name if bucket_name else self.config.s3.bucket

        bucket = self.bo_res.Bucket(bucket_name)
        logger.debug("Found bucket: %s", bucket_name)

        if file_name and not sub_folder:
            download_file = self.download_exp_file(bucket, file_name)
            return [download_file]

        files = []
        if sub_folder:
            objects = bucket.objects.filter(Prefix=sub_folder)
            if len(list(objects)) == 0:
                raise ValueError(f"Folder:{sub_folder} does not exist in s3")
        else:
            objects = bucket.objects.all()

        for obj in objects:
            if obj.key.endswith("/"):
                os.makedirs(self.abs_download_folder + obj.key, exist_ok=True)
                continue
            if file_name and file_name in obj.key:
                download_file = self.abs_download_folder + file_name
                bucket.download_file(obj.key, download_file)
                files.append(download_file)
                break
            else:
                download_file = self.abs_download_folder + obj.key
                bucket.download_file(obj.key, download_file)
                files.append(download_file)

        logger.debug("Downloaded all files %s", str(files))
        return files

    def download_exp_file(self, bucket, file_name):
        """Download a file that exists in s3
        file_name can contain full path as in s3,
        :raise error when the file is missing """
        download_file = self.abs_download_folder + file_name
        logger.debug("Downloading expected file: %s", download_file)
        dirs, filename = os.path.split(download_file)
        os.makedirs(dirs, exist_ok=True)
        try:
            bucket.download_file(file_name, download_file)
        except ClientError as ex:
            logger.error("Missing specified file: " + file_name, exc_info=ex)
            raise
        return download_file

    def move(self, req: MoveFileReq, del_src: Optional[bool] = True):
        """move s3 file to another file in same or different bucket
        :param MoveFileReq: contains optional buckets, src_file and target_file
        :param del_src: to delete the src file, default is to be deleted
        :return: True if file was moved
        """

        if not req.bucket_src and not req.bucket:
            req.bucket_src = self.config.s3.bucket
            req.bucket = self.config.s3.bucket

        copy_src = {
            'Bucket': req.bucket_src,
            'Key': req.file_src
        }
        # Upload the file
        try:
            self.bo_res.meta.client.copy(copy_src, req.bucket, req.file)
            if del_src:
                self.bo_res.meta.client.delete_object(Bucket=req.bucket_src,
                    Key=req.file_src)

        except ClientError as e:
            logging.error("Could not upload file: %s", req.file, exc_info=e)
            return False
        logger.info("Moved file: %s", str(req))
        return True


def get_client(config: models.S3ClientConfig) -> S3Client:
    """
    Get a RapidClient from configuration.
    """
    return S3Client(config)
