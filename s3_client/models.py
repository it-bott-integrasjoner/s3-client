from typing import List
from typing import List, Optional

from pydantic import BaseModel


class AwsConfig(BaseModel):
    aws_endpoint_url: str
    region_name: str
    aws_access_key_id: str
    aws_secret_access_key: str
    bucket: str
    download_path: str
    required_folders: List[str]


class MoveFileReq(BaseModel):
    bucket_src: Optional[str]
    bucket: Optional[str]
    file_src: str
    file: str


class S3ClientConfig(BaseModel):
    s3: AwsConfig
