from .client import get_client, S3Client
from .version import get_distribution

__all__ = ['S3Client']
__version__ = get_distribution().version


