# S3-client 

s3-client is a Python 3.x library for accessing Amazon s3 storage, a service offered by Amazon Web Services that provides object storage through web service interface. 


To consume the s3 web service, lib [Boto3](https://github.com/boto/boto3) is used.  

## Manually upload or download files 
To manually upload or donwload files this web app [1] can be used. 

This requires that the there a valid uio user when you have only guest user access 
there is need to login https://view.uio.no and then access the cmc.educlaud.no 

## Security
The project S3 project at uio is at start phase, it is not recommended to store any 
any secret information  
The S3 host, s3-oslo.educloud.no is public and accessible for all 
 
## Configuration:
To configure se the example.config.yml  
The api key and secret can be retrieved from the web app [1] 

## Contact persons: 
S3 storage is available through uio drift (still at testing phase)  
Safet Amedov: safet.amedov@usit.uio.no


[1] [web app](https://cmc.educloud.no)  
[2] s3 host: s3-oslo.educloud.no 